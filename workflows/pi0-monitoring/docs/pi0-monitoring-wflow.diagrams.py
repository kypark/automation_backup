from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: Pi0 monitoring", filename="pi0-monitoring-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("pi0-reco", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaP0 \n stage=Repack", width='1.6')
        gt_lock = locks("CondDBLockGT: \n EcalLaserAPDPNRatiosRcd \n  EcalPedestalsRcd", width='1.6')
        reco_files = file("runreco.py \n template.sub \n batch_script.sh")
        pi0_reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaP0/*/RAW")
    with Cluster("pi0-mon", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        mon_files = file("runmon.py")
        pi0_mon = multiJob("Pi0MonHandler \n prev_input=pi0-reco")
    with Cluster("pi0-moncml", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        moncml_files = file("runmoncml.py")
        pi0_moncml = multiJob("Pi0MonCmlHandler \n prev_input=pi0-mon")

    [t0_lock, gt_lock] >> pi0_reco
    pi0_reco >> pi0_mon >> pi0_moncml

#!/usr/bin/env python3
import subprocess
import sys
from typing import Dict
from os import path, system
from typing import Optional, List, Tuple
from ecalautoctrl import (
    JobCtrl,
    HandlerBase,
    prev_task_data_source,
    process_by_intlumi,
)
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

import glob
import json
import pickle
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np

import ROOT

ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning  # switch off 'Info in <TCanvas::Print>: blabla'


@prev_task_data_source
@process_by_intlumi(target=2000)
class EopCmlHandler(HandlerBase):
    """
    Execute all the steps to generate the eop monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(
        self,
        task: str,
        prev_input: str,
        deps_tasks: Optional[List[str]] = None,
        **kwargs,
    ):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument(
            "--plotsdir",
            dest="plotsdir",
            default=None,
            type=str,
            help="Base path of plot location on EOS",
        )

        self.submit_parser.add_argument(
            "--plotsurl",
            dest="plotsurl",
            default=None,
            type=str,
            help="Base url for plots",
        )

        self.resubmit_parser.add_argument(
            "--plotsdir",
            dest="plotsdir",
            default=None,
            type=str,
            help="Base path of plot location on EOS",
        )

        self.resubmit_parser.add_argument(
            "--plotsurl",
            dest="plotsurl",
            default=None,
            type=str,
            help="Base url for plots",
        )

    def HarnessLimits(self, harnessname):
        if "/" in harnessname:
            for token in harnessname.split("/"):
                if "IEta" in token:
                    return self.HarnessLimits(token)

        return (
            int(harnessname.split("_")[1]),
            int(harnessname.split("_")[2]),
            int(harnessname.split("_")[4]),
            int(harnessname.split("_")[5]),
        )

    def writeIC(self, icfilename: str = None, ICmap: Dict = None):
        """
        Write a set of IC from a single IOV into a txt file. EB only

        Output format: ix iy iz IC IC error

        :param icfilename: output file name.
        :param ICmap: dictionary (ieta, iphi) containing the IC values
        """
        with open(icfilename, "w") as icfile:
            for ieta, iphi_ICmap in ICmap.items():
                for iphi, IC in iphi_ICmap.items():
                    icfile.write("%i\t%i\t0\t%f\t0.\n" % (ieta, iphi, IC))

    def format_harness(self, harness):
        values = harness.split("_")
        return "$ {} < i\eta < {} \;\;\;  {} < i\phi < {} $ ".format(
            values[1], values[2], values[4], values[5]
        )

    def make_cumulative_plot(self, outputFolder: str):
        IOV_files = glob.glob(f"{outputFolder}/*/results.pkl")
        print(outputFolder)
        print(IOV_files)
        IOVs = []
        for IOV_file in IOV_files:
            with open(IOV_file, "rb") as file:
                IOV = pickle.load(file)
                IOVs.append(IOV)

        if len(IOVs) == 0:
            print("Could not find any IOV, quitting")
            raise Exception("No IOVs found")

        IOVs = sorted(IOVs, key=lambda k: k["IOV_info"]["runs_end"])
        # print(IOVs)

        harnesses = list(IOVs[0]["harness_map"].keys())
        # print(harnesses)

        harness_results = {}  # for each harness create a list of IOVs vs scales

        system(f"mkdir -p {self.opts.plotsdir}/cumulative_plots")

        for harness in harnesses[:]:
            iovs = np.array([])
            iovs_unc = np.array([])
            scales = np.array([])
            scales_unc = np.array([])
            labels = []
            for i, IOV in enumerate(IOVs):
                # iovs.append(IOV["IOV_info"]["time_end"])
                # iovs.append((IOV["IOV_info"]["time_end"] + IOV["IOV_info"]["time_end"]) / 2)
                iovs = np.append(iovs, i + 1)
                iovs_unc = np.append(iovs_unc, 0.2)
                scales = np.append(scales, IOV["harness_map"][harness][0])
                scales_unc = np.append(scales_unc, IOV["harness_map"][harness][1])
                mean_time = int(
                    (IOV["IOV_info"]["time_end"] + IOV["IOV_info"]["time_begin"]) / 2
                )
                labels.append(datetime.fromtimestamp(mean_time).strftime("%d/%m/%Y"))

            harness_results[harness] = [iovs, scales]

            fig, ax = plt.subplots(1, 1, figsize=(10, 7), dpi=100)

            ax.errorbar(
                iovs, scales, yerr=scales_unc, xerr=iovs_unc, fmt="o", label=harness
            )

            ax.margins(x=0.1, y=0.0)
            ymax = np.max(scales + scales_unc) * 1.02
            ymin = np.min(scales - scales_unc) * 0.98
            ax.set_ylim(ymin, ymax)
            ax.set_xlabel("Date", fontsize=14, loc="right")
            ax.set_ylabel("E/p scale (median)", fontsize=14, loc="top")
            ax.set_title(self.format_harness(harness), fontsize=20)
            ax.set_xticks(iovs)
            ax.set_xticklabels(labels, rotation=90)
            ax.grid()
            fig.savefig(
                f"{self.opts.plotsdir}/cumulative_plots/{harness}.png",
                bbox_inches="tight",
            )
            plt.close(fig)

    def strip_path(self, path: str) -> str:
        s = path.split("/")
        return "/".join([""] + list(filter(lambda k: k != "", s)))

    def process(self, files: List[str]) -> Tuple:
        runbasedir = "/".join(self.strip_path(files[0]).split("/")[:-1])
        print(runbasedir)
        run = runbasedir.split("/")[-1]

        # merge all the results
        with open(f"{runbasedir}/splitting.json") as file:
            job_dictionary = json.load(file)

        with open(f"{runbasedir}/results.pkl", "rb") as file:
            info = pickle.load(file)

        for jobid in job_dictionary.keys():
            with open(f"{runbasedir}/results_{jobid}.pkl", "rb") as file:
                tmp = pickle.load(file)
                for key in tmp.keys():
                    info["harness_map"][key] = tmp[key]

        print("Creating point corrections")

        # create runbaseplotsdir
        runbaseplotsdir = f"{self.opts.plotsdir}/{run}"
        process = subprocess.Popen(
            f"mkdir -p {runbaseplotsdir}", shell=True, stdout=subprocess.PIPE
        )
        process.wait()

        # save json in plotsdir for web display
        with open(f"{runbaseplotsdir}/results.json", "w") as fo:
            json.dump(info, fo, indent=2)

        with open(f"{runbasedir}/results.pkl", "wb") as fo:
            pickle.dump(info, fo)

        h2_ic = ROOT.TH2F(
            "eop_map", "E/p map; i#phi; i#eta", 360, 0.5, 360.5, 171, -85.5, 85.5
        )

        IOV_list = [
            [
                int(info["IOV_info"]["runs_begin"]),
                int(info["IOV_info"]["lumisection_begin"]),
                int(info["IOV_info"]["runs_end"]),
                int(info["IOV_info"]["lumisection_end"]),
            ]
        ]

        IC = {0: {}}  # IC is a list of dictionaries, i.e. IC [iIOV] [ix] [iy]
        iIOV = 0
        for ieta in range(-85, 86):
            IC[iIOV][ieta] = {}

        ref_scale = 1.0
        for harnessname in info["harness_map"].keys():
            IOV = IOV_list[iIOV]
            Escale = info["harness_map"][harnessname][0]
            ietamin, ietamax, iphimin, iphimax = self.HarnessLimits(harnessname)
            for ieta in range(ietamin, ietamax + 1):
                for iphi in range(iphimin, iphimax + 1):
                    if Escale <= 0.5 or Escale > 2.0:
                        ICvalue = ref_scale
                    else:
                        ICvalue = ref_scale / Escale
                    ibin = h2_ic.FindBin(iphi, ieta)
                    IC[iIOV][ieta][iphi] = ICvalue
                    h2_ic.SetBinContent(ibin, ICvalue)

        print("Drawing histos")
        # plotdir = f"{runbasedir}/plots/"
        # system(f"mkdir -p {plotdir}")
        c = ROOT.TCanvas(
            "c_%s" % info["IOV_info"]["runs_end"],
            "c_%s" % info["IOV_info"]["runs_end"],
            1400,
            700,
        )
        c.cd()
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetPalette(55)
        h2_ic.SetMinimum(0.80)
        h2_ic.SetMaximum(1.20)
        h2_ic.Draw("colz")
        c.Print(f"{runbaseplotsdir}/ic_map.png")
        c.Print(f"{runbaseplotsdir}/ic_map.root")

        print("writing output txt files")
        icdir = f"{runbaseplotsdir}/PointCorrections/"
        system(f"mkdir -p {icdir}")
        output = []
        with open(f"{icdir}/IOVdictionary.txt", "w") as IOVdict_file:
            for iIOV, IOV in enumerate(IOV_list):
                icfilename = f"{icdir}/IC_{IOV[0]}-{IOV[1]}_{IOV[2]}-{IOV[3]}.txt"
                output.append(path.abspath(f"{icfilename}"))
                IOVdict_file.write(
                    "%i\t%i\t%i\t%i\t%s\n"
                    % (IOV[0], IOV[1], IOV[2], IOV[3], icfilename)
                )
                self.writeIC(icfilename, IC[iIOV])

        self.make_cumulative_plot("/".join(runbasedir.split("/")[:-1]))

        return (
            [runbasedir + "/results.pkl"],
            f"{self.opts.plotsurl}/{run}/",
        )

    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """
        for (
            group
        ) in self.groups():  # grouping by int lumi, as specified in the decorator above
            # master run ---> to set the tag in the JobCtrl
            run = group[-1]
            fdict = self.get_files(group)  # get the files in the group
            if fdict is not None and len(fdict) > 0:
                jctrl = JobCtrl(
                    task=self.task,
                    campaign=self.campaign,
                    tags={"run_number": run["run_number"], "fill": run["fill"]},
                    dbname=self.opts.dbname,
                )
                if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                    files = jctrl.getJob(jid=0, last=True)[-1]["inputs"]
                    try:
                        jctrl.running(jid=0)

                        ret = self.process(
                            files=files,
                        )
                        if not ret:
                            jctrl.failed(jid=0)
                        else:
                            # mark as completed
                            jctrl.done(
                                jid=0,
                                fields={"output": ",".join(ret[0]), "plots": ret[1]},
                            )  # actually no plots are drawn now, to be implemented
                    except Exception as e:
                        print("Exception")
                        jctrl.failed(jid=0)
                        self.log.error(
                            f'Failed producing the IC for run:  {run["run_number"]}: {e}'
                        )
                        continue

    def submit(self) -> int:
        """
        Submit calibration jobs for runs in eop-new
        """
        print("Submitting")
        for (
            group
        ) in self.groups():  # grouping by int lumi, as specified in the decorator above
            # master run ---> to set the tag in the JobCtrl
            run = group[-1]
            fdict = self.get_files(group)  # get the files in the group
            print(run, fdict)

            if fdict is not None and len(fdict) > 0:
                jctrl = JobCtrl(
                    task=self.task,
                    campaign=self.campaign,
                    tags={"run_number": run["run_number"], "fill": run["fill"]},
                    dbname=self.opts.dbname,
                )

                if not jctrl.taskExist():
                    print("Creating task")
                    jctrl.createTask(
                        jids=[0],
                        fields=[
                            {
                                "group": ",".join(
                                    [r["run_number"] for r in group[:-1]]
                                ),
                                "inputs": ",".join([f.split(",")[-1] for f in fdict]),
                            }
                        ],
                    )

                try:
                    print("Set jctrl to running")
                    jctrl.running(jid=0)
                    self.rctrl.updateStatus(
                        run=run["run_number"], status={self.task: "processing"}
                    )
                    for r in group[:-1]:
                        self.rctrl.updateStatus(
                            run=r["run_number"], status={self.task: "merged"}
                        )

                    ret = self.process(
                        files=[f.split(",")[-1] for f in fdict],
                    )
                    if not ret:
                        print("At the end the job failed")
                        jctrl.failed(jid=0)
                    else:
                        print("Job completed!")
                        # mark as completed
                        jctrl.done(
                            jid=0, fields={"output": ",".join(ret[0]), "plots": ret[1]}
                        )
                        print(",".join(ret[0]))
                        print(ret[1])
                except Exception as e:
                    # jctrl.failed(jid=0)
                    self.rctrl.updateStatus(
                        run=run["run_number"], status={self.task: "reprocess"}
                    )
                    self.log.error(
                        f'Failed producing the IC for run:  {run["run_number"]}: {e}'
                    )
                    continue


if __name__ == "__main__":
    handler = EopCmlHandler(
        task="eop-cml",
        deps_tasks=["eop-mon"],
        prev_input="eop-mon",
    )
    ret = handler()
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EopCmlHandler)

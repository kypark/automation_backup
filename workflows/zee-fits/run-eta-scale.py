#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
from argparse import Namespace
import matplotlib.pyplot as plt
import numpy as np
from typing import Optional, List, Tuple
from ijazz import Config
from ijazz.bin.IJazZ import main as ijazz_main
from ijazz.bin.IJazZHDF import display_hdf as ijazz_plots
from ijazz.bin.IJazZEtaScale import eta_scale as ijazz_etascale
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_intlumi,process_by_run
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase


target_lumi = 2000

@prev_task_data_source
@process_by_intlumi(target=target_lumi)
class IJazZHandler(HandlerBase):
    """
    Execute all the steps to generate the Zee monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--config',
                                        dest='config',
                                        default=None,
                                        type=str,
                                        required=True,
                                        help='Mandatory IJazZ config file')
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
        self.submit_parser.add_argument('--ref_file',
                                        dest='ref_file',
                                        default=None,
#'/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_repro/zee-reference/ijazz.mc_126X_mcRun3_2023_forPU65_v6.hdf5',
                                        type=str,
                                        help='ijazz MC reference file')

        self.resubmit_parser.add_argument('--config',
                                        dest='config',
                                        default=None,
                                        type=str,
                                        required=True,
                                        help='Mandatory IJazZ config file')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')
        
        self.resubmit_parser.add_argument('--ref_file',
                                        dest='ref_file',
                                        default=None,
                                        type=str,
                                        help='ijazz MC reference file')   


    def resubmit(self) -> int:
        """
        Mark failed run for reprocessing.
        """

        # get the new run to process
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})
        # check if any job failed, if so mark all runs for reprocessing
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.rctrl.updateStatus(run=run_dict['run_number'], status={self.task : 'reprocess'})
                group = jctrl.getJob(jid=0, last=True)[-1]['group'] if 'group' in jctrl.getJob(jid=0, last=True)[-1] else ''
                if group:
                    for r in group.split(','):
                        self.rctrl.updateStatus(run=r, status={self.task : 'reprocess'})

        return 0

    def etascale_args(self,config_json_, outdir_, lumi_):
        """
        Prepare configuration for eta_scale calculation
        """
        ns= Namespace(config=config_json_,out=outdir_,lumi=lumi_,interactive=False)
        return ns

    def etascale_json(self,name,hd5path,refpath):
        """
        Prepare json for etascale calculation
        """

        dict = {}
        key = 'master_'+name
        group ={}  
        group['path']=hd5path
        group['name']=name
        group['lumi']=1     
        dict[key]= group

        mcgroup={}
        mcgroup['path']=refpath
        mcgroup['name']='MC'
        mcgroup['lumi']=1
        dict['ref']=mcgroup

        return dict

    def submit(self) -> int:
        """
        Submit new runs.

        :return: status.
        """


        main_cfg = Config(self.opts.config)
        main_cfg.use_absolute_path = True

        for group in self.groups():
            # main run
            run = group[-1]
            # ECALELF produces 4 output files, we only need the main one (the last one in the list)
            ecalelf_files = [lf.split(',')[-1] for lf in self.get_files(group)]
            if ecalelf_files is not None and len(ecalelf_files)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                if not jctrl.taskExist():
                    jctrl.createTask(jids=[0],
                                     fields=[{
                                         'group': ','.join([r['run_number'] for r in group[:-1]]),
                                         'inputs': ','.join([f.split(',')[-1] for f in ecalelf_files])}])
                try:
                    jctrl.running(jid=0)
                    self.rctrl.updateStatus(run=run['run_number'],
                                            status={self.task: 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'],
                                                status={self.task: 'merged'})
                                                
                    self.log.info(f'Processing fill: {run["fill"]}')
                    
                    # Run IJazZ routine
                    main_cfg.root_files = ecalelf_files
                    outpath_name = str(self.opts.eosdir)+'/'+str(run['run_number'])+'/'
                    outfile_name =outpath_name+'ijazz_results.hdf5' 
                    main_cfg.hdf = os.path.abspath(outfile_name)
                    ijazz_main(main_cfg)
                    ijazz_plots(main_cfg.hdf,True,None,None, self.opts.eosplots)
                    #second step: scale calculation  
                    cfg_json = self.etascale_json(run['run_number'], outfile_name, self.opts.ref_file)
                    etascale_cfg = self.etascale_args(cfg_json, outpath_name, target_lumi)  
                    ijazz_etascale(etascale_cfg)
                    
                    #copy relevant plots, brute force
                    plots_to_show = ['abs_scale_master_'+str(run['run_number'])+'.jpg',
                                     'eta_scale_master_'+str(run['run_number'])+'.jpg']
                    eosplots_dirname= self.opts.eosplots+'/'+str(run['run_number'])
                    os.makedirs(eosplots_dirname,exist_ok=True)  
                    for file in plots_to_show:
                        shutil.copy2(outpath_name+'/'+file,eosplots_dirname) 
                        
                    for r in group: 
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'done'})

                    jctrl.done(jid=0, fields={
                        'output': str(outpath_name),
                        'plots': str(self.opts.plotsurl) +'/'+str(run['run_number'])})

                except Exception as e:
                    for r in group:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'failed'})
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed runnin IJazZ with config {self.opts.config} on run {run["fill"]}: {e}')
              
                return 0

if __name__ == '__main__':
    handler = IJazZHandler(task='zee-eta-scale',                        
                           prev_input='ecalelf-ntuples-zskim')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(IJazZHandler)

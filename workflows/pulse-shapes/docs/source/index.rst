Automation workflow docs: ECAL pulse shapes
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runreco.py
==========

.. argparse::
   :filename: ../runreco.py
   :func: get_opts
   :prog: runreco.py

merger.py
=========

.. argparse::
   :filename: ../merger.py
   :func: get_opts
   :prog: merger.py

merger-fill.py
==============

.. argparse::
   :filename: ../merger-fill.py
   :func: get_opts
   :prog: merger-fill.py


runhltval.py
============

.. argparse::
   :filename: ../runhltval.py
   :func: get_opts
   :prog: runhltval.py
          
conddb_upload.py
================

.. argparse::
   :filename: ../conddb_upload.py
   :func: get_opts
   :prog: conddb_upload.py

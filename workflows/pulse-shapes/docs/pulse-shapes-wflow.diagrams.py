from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob 
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: pulse-shapes", filename="pulse-shapes-wflow", show=False, direction="LR") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("pulse-shape-reco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        file("runreco.py \n template.sub \n batch_script.sh")
    with Cluster("pulse-shape-merge", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        merge = singleJob("PSMergeHandlerRun \n @prev_task_data_source \n @process_by_run")
        file("merger.py")
    with Cluster("pulse-shape-merge-fill", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        merge_fill = singleJob("PSMergeHandlerFill \n @prev_task_data_source \n @process_by_fill")
        file("merger.py \n merger-fill.py")
    with Cluster("pulse-shape-hltref", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltref = multiJob("PSValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py")
    with Cluster("pulse-shape-hltval", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltval = multiJob("PSValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py")
    with Cluster("pulse-shapes-upload", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        db = singleJob("ConddbUploaderHandler \n @process_by_fill")
        file("runhltval.py")
    t0_lock >> reco >> merge >> merge_fill
    merge_fill >> hltref
    merge_fill >> hltval
    hltref >> db
    hltval >> db

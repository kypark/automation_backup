# Pulse shapes measurement

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/pulse-shapes-prod/) 
- [Pulse shapes analysis code](https://github.com/emanueledimarco/EcalReconstruction)
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/pulse-shapes)

## Workflow structure
The pulse shape workflow is depicted in the image below:

![wflow-fig](pulse-shapes-wflow.png)





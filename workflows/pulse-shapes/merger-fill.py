import sys
from typing import List, Optional
from ecalautoctrl import process_by_fill, prev_task_data_source
from merger import PSMergeHandler
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@prev_task_data_source
@process_by_fill(fill_complete=True)
class PSMergeHandlerFill(PSMergeHandler):
    """
    Run the PS production and validation task for each fill once the fill
    has been dumped.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=[],
                 **kwargs):
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)
        self.isfill = True

if __name__ == '__main__':
    handler = PSMergeHandlerFill(task='pulse-shapes-merge-fill',
                                 prev_input='pulse-shapes-merge')

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(PSMergeHandlerFill)

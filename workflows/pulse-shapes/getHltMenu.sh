#!/bin/bash

#set -x

INFILE=${1}
GT=${2}
TASK=${3}

# Monitor different paths for HI and pp runs
# Path names without "_vXX" suffix
# Path names must match the path names in batch_script_hltval.sh
if [[ $INFILE == *"/store/hidata/"* ]]; then
  ISHIRUN=true
  pathToMonitor=("HLT_HIEle20Gsf"
                 "HLT_HIGEDPhoton30"
                )
  HLTCUSTOMIZATION="--customise HLTrigger/Configuration/CustomConfigs.customiseHLTforHIonRepackedRAW"
else
  ISHIRUN=false
  pathToMonitor=("HLT_Ele32_WPTight_Gsf"
                 "HLT_Ele35_WPTight_Gsf"
                 "HLT_Ele38_WPTight_Gsf"
                 "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned"
                 "HLT_Photon33"
                 "HLT_PFMET120_PFMHT120_IDTight"
                )
  HLTCUSTOMIZATION=""
fi

pathsToInclude=""

# Make a comma separated list of paths to be included in the menu
for (( i = 0; i < ${#pathToMonitor[@]}; ++i )); do
  if [ $i -eq 0 ]; then
    pathsToInclude="${pathToMonitor[i]}_v*"
  else
    pathsToInclude="$pathsToInclude,${pathToMonitor[i]}_v*"
  fi
done

# If the DB name or the campaign contain "prompt" the HLT menu corresponding to the run number is used,
# assuming that the automation CMSSW release is compatible with it.
# Otherwise the GRun or HIon HLT menus corresponding to the automation CMSSW version is used.
DB=`echo $TASK | sed -E 's/.*--db ecal_(.*)_.*/\1/g'`
CAMPAIGN=`echo $TASK | sed -E 's/.* -c ([a-zA-Z1-9_-]+) .*/\1/g'`
if [ $DB == "prompt" ] || [[ $CAMPAIGN == *"prompt"* ]]; then
  RUN=`echo $TASK | sed -e 's/^.*run_number://g' | sed -e 's/,.*$//g'`
  HLTMENU="run:$RUN"
  HLTGT=""
  L1EMU=""
else
  HLTDEVRELEASE=`echo $CMSSW_VERSION | sed -E 's/(CMSSW_[0-9]+_[0-9]+_).*/\10/g'`
  if [ "$ISHIRUN" == true ]; then
    HLTMENU="/dev/$HLTDEVRELEASE/HIon"
  else
    HLTMENU="/dev/$HLTDEVRELEASE/GRun"
  fi
  HLTGT="auto:run3_hlt"
  L1EMU="--l1-emulator uGT"
fi

# Add ECAL specific modifications to hltGetConfiguration and run to get the HLT menu
source customizeHltGetConfiguration.sh
./hltGetConfigurationECAL $HLTMENU --output none --paths $pathsToInclude --eras Run3 $L1EMU --unprescale --max-events=-1 --taskstr "\"$TASK\"" --globaltag "$HLTGT" --dumpname=hlt.py $HLTCUSTOMIZATION


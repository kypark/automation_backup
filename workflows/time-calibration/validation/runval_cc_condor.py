#!/usr/bin/env python3
# collect all the files from the fill and create a configuration file
import sys
from TICMeanTimeHandlerCondor import TICMeanTimeHandlerCondor

if __name__ == '__main__':
    handler = TICMeanTimeHandlerCondor(task='timing-cc-val',
                                 deps_tasks=['timing-cc-reco'],
                                 prev_input='timing-cc-reco',
                                 is_cc=True,
                                 is_rereco=False)

    ret = handler()

    sys.exit(ret)

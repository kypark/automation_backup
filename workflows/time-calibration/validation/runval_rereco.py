#!/usr/bin/env python3
# collect all the files from the fill and create a configuration file
import sys
from TICMeanTimeHandler import TICMeanTimeHandler

if __name__ == '__main__':
    handler = TICMeanTimeHandler(task='timing-val-rereco',
                                 deps_tasks=['timing-rereco'],
                                 prev_input='timing-rereco',
                                 is_cc=False,
                                 is_rereco=True)

    ret = handler()

    sys.exit(ret)

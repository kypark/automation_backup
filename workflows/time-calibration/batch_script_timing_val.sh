#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}

EOSDIR=${5}     # base directory for dat, sqlite output 
WDIR=${6}       # base directory for python scripts
EOSPLOTS=${7}   # plot output directory
PLOTSURL=${8}   # plot output for output field

RUNS=${9}       # list of runs
FILL=${10}      # fill number for the runs
ISRERECO=${11}
ISCC=${12}
TAG=${13}
GT=${14}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# output directory
if [ "$ISRERECO" == "True" ]
then
    OUTDIR=$EOSDIR/validation-rereco/$FILL/
else
    OUTDIR=$EOSDIR/validation-reco/$FILL/
fi

mkdir -p $OUTDIR

# save payload (for reco)
if [ "$ISRERECO" != "True" ]
then
    for run_number in ${RUNS//,/ }
    do
        RUN="$run_number"            # get master run
    done 
    bash $WDIR/savePayload.sh $RUN $GT $ISCC $OUTDIR
fi    

# Produce dat files
RunTimeAverage --files=$INFILE --nSigma=2 --maxRange=10 --ebECut=0.5 --eeECut=0.5 --outputCalibCorr=ecalTiming-corr_$FILL.dat --outputFile=ecalTiming_$FILL.root --outputDir=$OUTDIR/

# Plot from the dat file
EOSPLOTS=$EOSPLOTS/validation/$FILL/
mkdir -p $EOSPLOTS

python3 $WDIR/plot_calibration.py $OUTDIR/ecalTiming-corr_$FILL.dat $ISRERECO $EOSPLOTS

# Save sqlite (for reco)
if [ "$ISRERECO" != "True" ]
then
    SQLITE=$OUTDIR/ecalTiming-abs_$FILL.db
    bash $WDIR/produceSqlite.sh $FILL $TAG $OUTDIR

    for run_number in ${RUNS//,/ }
    do
        DESTDIR=$EOSDIR/rereco/$run_number/
        mkdir -p $DESTDIR
        ln -sf $SQLITE $DESTDIR/ecalTiming-abs.db
    done
fi

RET=$?   # check the exit status of the previous command

# Remove stuff that will no longer be used
rm $OUTDIR/ecalTiming.dat
rm $OUTDIR/ecalTiming_$FILL.root

# Update output field
if [ "$RET" == "0" ]
then
    OUTNAME=$OUTDIR/ecalTiming-corr_$FILL.dat
    PLOTSURL=$PLOTSURL/validation/$FILL/
    
    if [ "$ISRERECO" != "True" ]
    then
        ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OUTNAME}" "plots:${PLOTSURL}" "sqlite:${SQLITE}"
    else
        ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OUTNAME}" "plots:${PLOTSURL}"
    fi
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET

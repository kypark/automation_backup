import FWCore.ParameterSet.Config as cms

# helper functions
from HLTrigger.Configuration.common import *

def customizeHLTforCCTiming(process):
    # make sure to run only on CPU because there is no CC timing implementation for GPU yet
    process.options.accelerators = ['cpu']

    for producer in producers_by_type(process, "EcalUncalibRecHitProducer"):
        producer.algoPSet = dict(timealgo = 'crossCorrelationMethod',
            EBtimeNconst = 25.5,
            EBtimeConstantTerm = 0.85,
            outOfTimeThresholdGain12pEB = 3.0,
            outOfTimeThresholdGain12mEB = 3.0,
            outOfTimeThresholdGain61pEB = 3.0,
            outOfTimeThresholdGain61mEB = 3.0,
            timeCalibTag = cms.ESInputTag('', 'CC'),
            timeOffsetTag = cms.ESInputTag('', 'CC')
        )

    return process


from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: time calibration", filename="time-calibration-wflow", show=False, direction="LR") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("timing-reco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runreco.py \n template.sub \n batch_script.sh")
        reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")
    with Cluster("timing-val", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        val = singleJob("TICMeanTimeHandler \n @prev_task_data_source \n @process_by_fill")
        file("validation/runval.py \n plot_calibrations.py \n produceSqlite.sh \n savePayload.sh")
    with Cluster("timing-rereco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        t0_lock_rereco = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runrereco.py \n template_rereco.sub \n batch_script_rereco.sh")
        rereco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")
    with Cluster("timing-val-rereco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        val_rereco = singleJob("TICMeanTimeHandler \n @prev_task_data_source \n @process_by_fill")
        file("validation/runval_rereco.py \n plot_calibrations.py")
    with Cluster("timing-hltref", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltref = multiJob("TimingValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py \n customizeHltGetConfiguration.sh \n customize_hltvalidation.py \n template_hltval.sub \n batch_script_hltval.sh")
    with Cluster("timing-hltval", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltval = multiJob("TimingValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py \n customizeHltGetConfiguration.sh \n customize_hltvalidation.py \n template_hltval.sub \n batch_script_hltval.sh")
    t0_lock >> reco >> val
    [val, t0_lock_rereco] >> rereco >> val_rereco
    val >> hltref
    val >> hltval

with Diagram("Worflow: time calibration CC timing", filename="time-calibration-cc-wflow", show=False, direction="LR") as d_cc:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("timing-cc-reco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runreco_cc.py \n template_cc.sub \n batch_script_cc.sh")
        reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")
    with Cluster("timing-cc-val", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        val = singleJob("TICMeanTimeHandler \n @prev_task_data_source \n @process_by_fill")
        file("validation/runval_cc.py \n plot_calibrations.py \n produceSqlite.sh \n savePayload.sh")
    with Cluster("timing-cc-rereco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        t0_lock_rereco = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runrereco_cc.py \n template_rereco_cc.sub \n batch_script_rereco_cc.sh")
        rereco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")
    with Cluster("timing-cc-val-rereco", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        val_rereco = singleJob("TICMeanTimeHandler \n @prev_task_data_source \n @process_by_fill")
        file("validation/runval_rereco_cc.py \n plot_calibrations.py")
    with Cluster("timing-cc-hltref", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltref = multiJob("TimingValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py \n customizeHltGetConfiguration.sh \n customize_hltvalidation.py \n customizeHLTForCCTiming.py \n template_hltval_cc.sub \n batch_script_hltval.sh")
    with Cluster("timing-cc-hltval", direction="TB"):
        Node("", width='2.5', height='0', style='invisible')
        hltval = multiJob("TimingValidationHandler \n @dbs_data_source \n @process_by_fill \n dsetname=/HLTPhysics*/*/RAW \nor /HIHLTPhysics*/*/RAW")
        file("runhltval.py \n customizeHltGetConfiguration.sh \n customize_hltvalidation.py \n customizeHLTForCCTiming.py \n template_hltval_cc.sub \n batch_script_hltval.sh")
    t0_lock >> reco >> val
    [val, t0_lock_rereco] >> rereco >> val_rereco
    val >> hltref
    val >> hltval

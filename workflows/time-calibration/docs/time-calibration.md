# Time calibration

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/timing-prod/) 
- [Calibration code](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/EcalTiming/tree/automation)
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/time-calibration)

## Workflow structure
The time calibration runs on a dedicated stream that stores only ECAL digis from ZeroBias events. The stream is saved in a RAW dataset:

- `/AlCaPhiSym/*/RAW`

The first step of the calibration runs the reconstruction in two different versions. On version uses the RatioMethod timing reconstruction algorithm and the other one uses the crossCorrelationMethod timing algorithm and tighter kOutOfTime thresholds in EB.

The validation task calculates the timing calibration constants and produces the sqlite file with the conditions.

The timing-rereco task re-runs the reconstruction with the new timing conditions.

The timing-val-rereco task recalculates the timing calibrations constants as a closure test. The timing should be centred sharply around zero.

The HLT validation tasks timing-hltref and timing-hltval run selected HLT paths with the conditions in the GT (ref) and with the new conditions (val) and calculate the ratio of the unprescaled rates of the two.

The automation workflow structures for the ratio timing and CC timing workflows can be seen in the diagrams below.

![wflow-fig](time-calibration-wflow.png)
![wflow-fig](time-calibration-cc-wflow.png)





#!/usr/bin/env python3
import os, sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock

if __name__ == '__main__':
    t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack')

    handler = HTCHandlerByRunDBS(task='timing-cc-rereco',
                                 dsetname='/AlCaPhiSym/*/RAW',
                                 deps_tasks=['timing-cc-val'],
                                 locks=[t0lock])

    ret = handler()

    sys.exit(ret)


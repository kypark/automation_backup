* ECAL Automation repository
** How to use the repository
*** General recommendations:
    - The =master= branch contains all workflows and serves base for the =automation:dev= image
    - Groovy utilities are collected in the shared library branch =ectrl-shared-lib=
      
*** New workflow
    - Developer with push rights creates a new directory under =workflows= to contain all workflow related code and documentation
    - user forks the project on [[https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation][GitLab]].
    - user clones the repository
      #+BEGIN_EXAMPLE
      git clone ssh://git@gitlab.cern.ch:7999/USERNAME/automation.git
      #+END_EXAMPLE
    - Starting from the master Jenkinsfile, modify the =stages= section to process your workflow (you can use the other branches as inspiration).
    - Push the new branch to your GitLab repository.
    - Open a merge request on GitLab.


** How to monitor a task execution
*** Jenkins
    - The status of different campaigns for different workflows can be found on the [[https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Online/][Jenkins webpage]]

*** Jobs monitoring
    - Every job logs its status to a dedicated influxb instance
    - Jobs/tasks statuses can be viewed on [[https://ecal-automation-monit.web.cern.ch][Grafana]]:
      + Details for a single task can be inspected by selecting =campaign=, =fill=, =run=, =workflow= from the variables panel.

** PROD vs DEV
   Two Jenkins items (workflows) should be created for each branch: production and development.
   Production workflows are marked as such by appending the suffix ='-prod'= to the Jenkins
   item name. This allow the relative environmental variables to be set in the Jenkinsfile by
   simply calling the =setenv= function defined in the groovy shared library:

   #+BEGIN_EXAMPLE
   import static ectrl.vars.GlobalVars.*
   setenv(this, env.JOB_BASE_NAME)
   #+END_EXAMPLE

   Production items should be configured such that a build is trigger at regular intervals.
   They should also point to a specific tag, by convention named after the branch+"=-prod=" (=branch-name-prod=).
   When developing a new production version should be pushed using the following commands:

   #+BEGIN_EXAMPLE
   git tag -f master-prod
   git push -f --tags master-myprocess
   #+END_EXAMPLE

   Note that both the tag creation and push to the repository are forced (=-f=).

** Docker images
   Docker images are built by the GitLab CI from the =master= branch and the =master-prod= tag.
   A description of the content and structure of the image can be found in the [[https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/docker-images][branch README]].
   
** Tools
   Monitoring and other tools are also hosted in this repository as separate branch
   (=online-laser-mon=, =eos-monitoring= for instance). 

